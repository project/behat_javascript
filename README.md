# Behat javascript

This module aims to provide tools to work with Behat tests and Javascript in Drupal projects.

## Features included

- Shows JS errors on Behat scenarios and marks them as failed

## Requirements

The only requirement is to have drupal/drupalextension configured in your project. [More info](https://github.com/jhedstrom/drupalextension).


## Installation

To install the module just require and enable it:

```bash
composer require drupal/behat_javascript
drush en behat_javascript
```

## Usage

When the behat tests are run, JS errors will be shown and mark the scenario as failed.