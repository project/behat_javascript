window.jsErrors = [];

window.onerror = function (errorMessage, filename, lineno, colno, error) {
  if (error instanceof Error) {
    window.jsErrors[window.jsErrors.length] = 'Filename: ' + filename + ' ; Error: ' + errorMessage + ' ; Lineno: ' + lineno + ' ; Colno: ' + colno;
  }
  else {
    window.jsErrors[window.jsErrors.length] = errorMessage;
  }

};
