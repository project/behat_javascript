<?php

use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Mink\Mink;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalDriverManager;

/**
 * Catches js errors and reports them.
 *
 * @see https://github.com/25th-floor/behat-js-errorlog
 */
class BehatJavascriptContext extends DrupalSubContextBase {

  const IGNORE_TAG = 'ignore-js-error';

  /**
   * Used to manage driver and detect selenium 2.
   *
   * @var \Behat\Mink\Mink
   */
  private $mink;

  /**
   * Is context enabled on this scenario.
   *
   * @var bool
   */
  private $lookForJavascriptErrors = FALSE;

  /**
   * Scenario Information for error output.
   *
   * @var string
   */
  private $scenarioData;

  /**
   * List of errors that will be ignored.
   *
   * @var string[]
   */
  protected $ignoredErrors = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(DrupalDriverManager $drupal) {
    parent::__construct($drupal);
    $settings = \Drupal::config('behat_javascript.settings');
    $this->ignoredErrors = array_filter(explode("\n", $settings->get('ignored_errors') ?? ''));
  }

  /**
   * Sets Mink instance.
   *
   * This method is overriden as mink property is private.
   *
   * @param \Behat\Mink\Mink $mink
   *   Mink session manager.
   */
  public function setMink(Mink $mink) {
    $this->mink = $mink;
  }

  /**
   * Returns Mink instance.
   */
  public function getMink() {
    return $this->mink;
  }

  /**
   * Check if we should enable looking for js errors.
   *
   * @BeforeScenario @javascript
   */
  public function prepare(BeforeScenarioScope $scope) {
    if ($this->getMink() == NULL) {
      return;
    }

    if ($this->getMink()->getDefaultSessionName() != 'selenium2') {
      return;
    }

    // No need to do something if this is an empty scenario.
    if (!$scope->getScenario()->hasSteps() && !$scope->getFeature()->hasBackground()) {
      return;
    }

    // Ignore scenarios with the ignore tag.
    if ($scope->getScenario()->hasTag(self::IGNORE_TAG) || $scope->getFeature()->hasTag(self::IGNORE_TAG)) {
      return;
    }

    $this->lookForJavascriptErrors = TRUE;
    $this->scenarioData = basename($scope->getFeature()->getFile()) . '.' . $scope->getScenario()->getLine();
  }

  /**
   * Show an error when a JS error is found.
   *
   * @AfterStep
   */
  public function lookForJavascriptErrors(AfterStepScope $scope) {
    if (!$this->lookForJavascriptErrors || !$this->getSession()->isStarted()) {
      return;
    }

    $driver = $this->getMink()->getSession()->getDriver();

    try {
      $errors = $driver->evaluateScript("return window.jsErrors") ?? [];
    }
    catch (\Exception $e) {
      // Output where the error occurred for debugging purposes.
      echo $this->scenarioData;
      throw $e;
    }

    $errors = array_filter($errors, function ($error) {
      foreach ($this->ignoredErrors as $ignoredError) {
        if (preg_match(sprintf('/%s/', $ignoredError), $error)) {
          return FALSE;
        }
      }
      return TRUE;
    });

    if (!$errors || empty($errors)) {
      return;
    }

    $file = sprintf("%s:%d", $scope->getFeature()->getFile(), $scope->getStep()->getLine());
    $message = sprintf("Found %d javascript error%s", count($errors), count($errors) > 0 ? 's' : '');

    echo '-------------------------------------------------------------' . PHP_EOL;
    echo $file . PHP_EOL;
    echo $message . PHP_EOL;
    echo '-------------------------------------------------------------' . PHP_EOL;

    foreach ($errors as $index => $error) {
      echo sprintf("   #%d: %s", $index, $error) . PHP_EOL;
    }

    throw new \Exception($message);
  }

}
