<?php

namespace Drupal\behat_javascript\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Behat javascript settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'behat_javascript_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['behat_javascript.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['ignored_errors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Error messages to ignore'),
      '#default_value' => $this->config('behat_javascript.settings')->get('ignored_errors'),
      '#description' => $this->t('Add here the text or regular expressions that refer to errors that are not wanted to be displayed. Each ignored error must be separated by lines.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('behat_javascript.settings')
      ->set('ignored_errors', $form_state->getValue('ignored_errors'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
